using System.Collections.Generic;

namespace HumbleProgrammer.Characters
{
    public class MyCharacter : Character
    {
        private readonly int _wins;
        public MyCharacter(int wins)
        {
            _wins = wins;
            Hp = _wins * 5;
        }

        public string Name { get; } = "MyCharacter"; 
        public int Cooldown { get; } = 5;

        public int Damage => _wins;

        public int Hp
        {
            get;
            set;
        }

        public int Speed => _wins * 10;
        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            var specialActionLog = new List<string>(); 
            
            foreach (var enemy in enemies)
            {
                if (enemy.Hp > 0) enemy.Hp -= 50;
                specialActionLog.Add(this.Name + " damaged " + enemy.Name + " to " + enemy.Hp);
            }

            foreach (var ally in allies)
            {
                if (ally.Hp > 0) ally.Hp += 10;
                specialActionLog.Add(this.Name + " healed " + ally.Name + " to " + ally.Hp);
            }
            
            return specialActionLog;
        }
    }
}