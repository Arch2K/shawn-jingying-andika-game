using System;
using System.Collections.Generic;

namespace HumbleProgrammer.Characters
{
    public class OpChar : Character
    {
        private int _wins; 
        public OpChar(int wins)
        {
            if (wins < 0)
            {
                throw new Exception("Impossible");
            }
            _wins = wins;
            Hp = wins * 100 + 10000;
        }
        
        public string Name => "OP";

        public int Cooldown => _wins * 1 + 2;

        public int Damage => _wins * 10 + 1000;

        public int Hp { get; set; }

        public int Speed => _wins * 10 + 10;
        

        //returns a string array of battle logs to be appended to battle logs in Game
        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            List<string> specialActionLog = new List<string>();
            foreach (Character c in enemies)
            {
                if (c.Hp <= 0)
                {
                    c.Hp = 500;
                    specialActionLog.Add($"{this.Name} revived {c.Name} for 500HP!");
                }
            }

            return specialActionLog;
        }
    }
    
}