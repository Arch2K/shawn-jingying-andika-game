using System;
using System.Collections.Generic;

namespace HumbleProgrammer.Characters
{
    public class myChar : Character
    {
        private int _wins; 
        public myChar(int wins)
        {
            if (wins < 0)
            {
                throw new Exception("Impossible");
            }
            _wins = wins;
            Hp = wins * 100 + 1000;
        }
        
        public string Name => "bob";

        public int Cooldown => _wins * 2 + 5;

        public int Damage => _wins * 10 + 100;

        public int Hp { get; set; }

        public int Speed => _wins * 5 + 10;
        

        //returns a string array of battle logs to be appended to battle logs in Game
        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            List<string> specialActionLog = new List<string>();
            foreach (Character c in allies)
            {
                c.Hp += 500;
                specialActionLog.Add(this.Name + " healed " + c.Name + " to " + c.Hp + "HP");
            }

            return specialActionLog;
        }
    }
    
}
