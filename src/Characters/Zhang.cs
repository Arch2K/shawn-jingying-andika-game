using System;
using System.Collections.Generic;
using System.Data;

namespace HumbleProgrammer.Characters
{
    public class Zhang : Character
    {
        
        private readonly int _wins;

        public Zhang(int wins)
        {
            if (wins < 0)
            {
                throw new ArgumentException();
            }
            
            _wins = wins;
        }

        public string Name { get; } = "Zhang";
        public int Cooldown => 5 * _wins;
        public int Damage => 1000 * _wins; 

        public int Hp { get; set; } = 100000;

        public int Speed => 10000 * _wins;

        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            if (enemies.Count > 1)
            {
                if (enemies[1].Hp <= 0)
                {
                    List<string> log = new List<string>{$"Character is already dead. {Name}'s Special Attack has no effect."};
                    return log;
                }
                else
                {

                    enemies[1].Hp = 0;
                    List<string> log = new List<string>{$"{enemies[1].Name} was killed by {Name}."};
                    return log;
                }
            }
            else
            {
                List<string> log = new List<string>{"No character to use Special Attack on."};
                return log;
            }

        }

    }
}