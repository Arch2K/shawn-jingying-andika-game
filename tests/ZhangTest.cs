using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using HumbleProgrammer.Characters;

namespace Tests
{
    public class ZhangTest
    {
    
        Zhang Jerry = new Zhang(2);
        
        [Fact]
        public void TestCooldown()
        {
            Jerry.Cooldown.Should().Be(10);
        }
        
        [Fact]
        public void TestDamage()
        {
            Jerry.Damage.Should().Be(2000);
        }
        
        [Fact]
        public void TestHp()
        {
            Jerry.Hp -= 1;
            Jerry.Hp.Should().Be(99999);
        }

        [Fact]
        public void TestSA()
        {
            MockCharacter m1 = new MockCharacter(30);
            MockCharacter m2 = new MockCharacter(40);
            List<Character> mockArray = new List<Character>();
            mockArray.Add(m1);
            mockArray.Add(m2);
            Jerry.SpecialAction(mockArray, mockArray);
            mockArray[1].Hp.Should().Be(0);
            mockArray[0].Hp.Should().Be(10);
        }
        
        [Fact]
        public void TestSAInvalid()
        {
            MockCharacter m1 = new MockCharacter(20);
            List<Character> mockArray = new List<Character>();
            mockArray.Add(m1);
            Jerry.SpecialAction(mockArray, mockArray).Should().BeEquivalentTo("No character to use Special Attack on.");
        }
    }

    public class MockCharacter : Character
    {
        private int _hp;
        private int _speed;

        public MockCharacter(int Speed)
        {
            _speed = Speed;
            _hp = 10;
        }
        public string Name { get; }
        public int Cooldown { get; } = 4;
        public int Damage { get; } = 10;

        public int Hp
        {
            get => _hp;
            set => _hp = value;
        }

        public int Speed { get => _speed; set => _speed = value; }
        public List<string> SpecialAction(List<Character> allies, List<Character> enemies)
        {
            if (enemies.Count > 1)
            {
                if (enemies[1].Hp <= 0)
                {
                    List<string> log = new List<string>{$"Character is already dead. {Name}'s Special Attack has no effect."};
                    return log;
                }
                else
                {

                    enemies[1].Hp = 0;
                    List<string> log = new List<string>{$"{enemies[1].Name} was killed by {Name}."};
                    return log;
                }
            }
            else
            {
                List<string> log = new List<string>{"No character to use Special Attack on."};
                return log;
            }

        }
    }
}