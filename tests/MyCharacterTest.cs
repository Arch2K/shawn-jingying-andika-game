using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using HumbleProgrammer.Characters;

namespace Tests
{
    public class CharacterTest
    {
        MyCharacter _myCharacter = new MyCharacter(20);
            
        [Fact]
        public void CooldownAssertion()
        {
            _myCharacter.Cooldown.Should().Be(5);
        }

        [Fact]
        public void SpeedAssertion()
        {
            _myCharacter.Speed.Should().Be(200);
        }

        [Fact]
        public void DamageAssertion()
        {
            _myCharacter.Damage.Should().Be(20);
        }

        [Fact]
        public void HpAssertion()
        {
            _myCharacter.Hp.Should().Be(100);
        }

        private List<Character> allies = new List<Character>();
        private List<Character> enemies = new List<Character>(); 
        
        [Fact]
        public void SpecialAssertion()
        {
            allies.Add(new MyCharacter(20));
            allies.Add(new MyCharacter(15));
            allies.Add(new MyCharacter(10));
            
            enemies.Add(new MyCharacter(20));
            enemies.Add(new MyCharacter(15));
            enemies.Add(new MyCharacter(10));
            
            List<string> specialLogs = _myCharacter.SpecialAction(allies, enemies);
            
            allies[0].Hp.Should().Be(110);
            allies[1].Hp.Should().Be(85);
            allies[2].Hp.Should().Be(60);

            enemies[0].Hp.Should().Be(50);
            enemies[1].Hp.Should().Be(25);
            enemies[2].Hp.Should().Be(0);
        }
        
        

    }
}